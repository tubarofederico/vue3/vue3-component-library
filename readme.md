
# Vue 3 ComponentsLibrary

## Estructura del proyecto

Este proyecto permite compilar componentes de Vue3 + Vite+ Composición + Typescript + Bootstrap 5 para ser compartidos por medio de NPM entre proyectos.

En el directorio **sandbox** se encuentra un proyecto Vue 3 convencional donde se pueden testear y previsualizar en tiempo real los componentes que se esten desarrollando.

En el directorio **scss** se encuentran todos los archivos de estilos de cada componente en donde sea necesario modificar los estilos de Bootstrap 5.

En el directorio **src** se encuentran todos los componentes organizados en directorios.

## Pre-requisitos

El componente uploader de archivos requiere que el endpoint en PHP tenga habilitada la extensión:

~~~ 
extension=php_fileinfo.dll
~~~

### Es necesario tener instalado

* [Node Js LTS](https://nodejs.org/en/) La versión utilizada para el primer desarrollo fue 18.13.0 LTS.
* [yarn](https://yarnpkg.com/) 

    ~~~ 
    npm install -g yarn
    ~~~

### Recomendaciones opcionales para instalar

* [Visual Studio Code](https://code.visualstudio.com/)
* [Vue Language Features (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.volar) - Extensión para Visual Studio Code.
* [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin) - Extensión para Visual Studio Code.
* [CMDER](https://github.com/cmderdev/cmder) es una consola de comandos sencilla que interpreta los comandos de linux en windows.

## Instalación

Clonar el proyecto desde **git**, con el comando en cualquier directorio de Windows:

~~~
    git clone https://gitlab.hcdiputados-ba.gov.ar/desarrollo/librerias/vue3-components-library.git .
~~~

Posteriormente realizar un:

~~~
    yarn install
~~~

## Compilación desarrollo

El comando **dev** compila el proyecto y crea un servidor local con hot reload para trabajar en tiempo real.

~~~
    yarn dev
~~~

## Compilación productiva y publicación en NPM

### Compilación producción

El comando **build** compila el proyecto comprimido para luego poder ser distribuido.

~~~
    yarn build
~~~

Si el comando anterior falla con errores de FilePond se debe realizar los siguientes pasos:

1) Comentar el contenido del archivo **node_modules\vue-filepond\types\index.d.ts** y agregar la linea:

~~~
    declare module 'vue-filepond';
~~~

2) Comentar el contenido del archivo **node_modules\filepond-plugin-file-validate-type\types\index.d.ts** y agregar la linea:

~~~
    declare module 'filepond-plugin-file-validate-type';
~~~

3) Comentar el contenido del archivo **node_modules\filepond-plugin-image-preview\types\index.d.ts** y agregar la linea:

~~~
    declare module 'filepond-plugin-image-preview';
~~~

4) Comentar el contenido del archivo **node_modules\filepond-plugin-file-validate-size\types\index.d.ts** y agregar la linea:

~~~
    declare module 'filepond-plugin-file-validate-size';
~~~

### Publicación NPM PRODUCCIÓN

Posteriormente para poder publicar la librería en [NPM](https://www.npmjs.com/) se debe:

1) Tener un usuario personal registrado en NPM. [Ir al registro...](https://www.npmjs.com/signup)

2) Acceder a la cuenta de NPM de la HDC desde el navegador. Credenciales:

    > Usuario: desarrollohcd

    > Contraseña:  "la misma que se utiliza en el correo hcddesarrolladoras@gmail.com"

3) Desde la cuenta de **desarrollohcd** Invitar al usuario registrado en el paso 1 a la organización **@hcd-bsas** desde el siguiente enlace:

    [https://www.npmjs.com/org/hcd-bsas/invite?track=existingOrgAddMembers](https://www.npmjs.com/org/hcd-bsas/invite?track=existingOrgAddMembers)


4) Autenticar solo una vez con el usuario registrado en el paso 1 desde la linea de comandos. Si ya se encuentra autenticado en la consola este paso ya no es necesario.

~~~
    npm login
~~~

5) Compilar la librería en modo producción:

~~~
    yarn build
~~~

6) Publicar librería con el comando **publish**, el cual preguntara cual es el nuevo número de versión. En donde "version": *.*.* --> El primer número corresponde a cambios grandes, el segundo nuevas funcionalidades y el tercero corrección de errores o cambios menores.

~~~
    npm publish
~~~

NOTA: Se puede conocer la ultima versión productiva desde el siguiente enlace: [Listado de versiones](https://www.npmjs.com/package/@hcd-bsas/vue3-components?activeTab=versions).

Nota: En ocasiones da el error "You cannot publish over the previously published versions" al no poder cambiar la versión de la librería desde la linea de comandos. Para ello se debe aumentar el número de versión en la opción **version** del package.json

### Publicación NPM VERSION BETA 

1) Seguir los pasos previamente descriptos para publicar una versión productiva del 1 al 6, agregando al comando **npm publish** el parametro **--tag beta** y modificar la version en el archivo package.json incrementando en 1 el número de la ultima versión beta.

~~~
    npm publish --tag beta
~~~

NOTA: Se puede conocer la ultima versión beta desde el siguiente enlace: [Listado de versiones](https://www.npmjs.com/package/@hcd-bsas/vue3-components?activeTab=versions).
 
### Como hacer uso de la librería con NPM en un proyecto nuevo

Pero para poder instalar la librería en cualquier otro proyecto se debe correr el comando en el directorio donde el proyecto contenga las librerías javascript.

~~~
    yarn add @hcd-bsas/vue3-components
~~~

Luego es necesario importar los estilos en un archivo .scss

~~~
    @import "../../node_modules/@hcd-bsas/vue3-components/scss/global.scss";
~~~

### Como actualizar la libreria

Si un proyecto se encuentra usando una versión previa de la libreria se puede actualizar todas las dependencias con el comando:

~~~
    yarn upgrade --force --latest
~~~

O, si se quiere actualizar a una versión en particular o volver a una versión previa se puede correr siguiente comando cambiando el número de version despues del @:

~~~
    yarn add @hcd-bsas/vue3-components@1.0.41
    yarn add @hcd-bsas/vue3-components@1.0.39-beta.1.0.14
~~~


### DOCUMENTACIÓNES OFICIALES

* #### [Vue 3](https://vuejs.org/guide/introduction.html)
* #### [Vite](https://vitejs.dev/config/)
* #### [Bootstrap 5](https://getbootstrap.com/docs/5.3/getting-started/introduction/)
* #### [Pinia](https://pinia.vuejs.org/introduction.html)
* #### [Vue Router](https://router.vuejs.org/guide/)