import App from './App.vue'
import { createApp } from 'vue'
import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import { createPinia } from 'pinia'
import "bootstrap";
import 'bootstrap-icons/font/bootstrap-icons.css'
import '../scss/global.scss';

import Swal from "sweetalert2/dist/sweetalert2.js";

import { BreadcrumbPlugin, DocumentsListPlugin } from "../src/index";

// Views
import BreadCrumbView from './views/breadcrumb/BreadCrumbView.vue';
import FileUploaderView from './views/uploader/FileUploaderView.vue';
import AutocompleteView from './views/autocomplete/Autocomplete.vue';
import DocumentsListView from './views/documentslist/DocumentsListView.vue';
import ModalView from './views/modal/ModalView.vue';
import InputExpedienteView from './views/inputExpediente/InputExpedienteView.vue';
import InputCurrencyView from './views/inputCurrency/InputCurrencyView.vue';
import TestView from './views/TestView.vue';
import InputCuilView  from './views/InputCuil/InputCuilView.vue';

const pinia = createPinia();

const routes: Readonly<RouteRecordRaw[]> = [
    { path: '/', name: "inicio", component: TestView, meta: { name: "Inicio" }},
    { path: '/uploader', name: "uploader", component: FileUploaderView, meta: { name: "Uploader" } },
    { path: '/breadcrumb', name: "breadcrumb", component: BreadCrumbView, meta: { name: "Breadcrumb" } },
    { path: '/breadcrumb/test', name: "breadcrumb_test", component: BreadCrumbView, meta: { name: "Breadcrumb Test" } },
    { path: '/autocomplete', name: "autocomplete", component: AutocompleteView, meta: { name: "Autocomplete" } },
    { path: '/documents-list', name: "documentslist", component: DocumentsListView, meta: { name: "Listado de documentos" } },
    { path: '/modal', name: "modal", component: ModalView, meta: { name: "Modal" } },
    { path: '/input-expediente', name: "input-expediente", component: InputExpedienteView, meta: { name: "Input Expediente" } },
    { path: '/input-currency', name: "input-currency", component: InputCurrencyView, meta: { name: "Input Currency" } },
    { path: '/input-cuil', name: "input-cuil", component: InputCuilView, meta: { name: "Input Cuil" } },
];

const router = createRouter({
    history: createWebHistory(),
    routes, 
});

const app = createApp(App);

app.use(router);
app.use(pinia);
app.use(BreadcrumbPlugin, {router: router });
app.use(DocumentsListPlugin, {
    base_url: "http://localhost/admin",
    sweetalert: Swal
});
app.mount('#app')