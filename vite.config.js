import { fileURLToPath, URL } from "node:url";
import { resolve } from 'node:path'
import { defineConfig } from 'vite'
import Vue from '@vitejs/plugin-vue'
import pkg from './package.json';

export default defineConfig({
  // If our .vue files have a style, it will be compiled as a single `.css` file under /dist.
  plugins: [Vue()],
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
    }
  },
  build: {
    // Output compiled files to /dist.
    outDir: './dist',
    lib: {
      // Set the entry point (file that contains our components exported).
      entry: [
        // resolve(__dirname, 'src/components/index.ts'),
        // resolve(__dirname, 'src/helpers/Shared.ts')
        resolve(__dirname, 'src/index.ts')
      ],
      // Name of the library.
      name: 'hcd-component-library',
      // We are building for CJS and ESM, use a function to rename automatically files.
      // Example: hcd-component-library.esm.js
      fileName: (format) => `${'hcd-component-library'}.${format}.js`,
    },
    rollupOptions: {
      // Vue is provided by the parent project, don't compile Vue source-code inside our library.
      external: Object.keys(pkg.dependencies || {}),
      output: {
        globals: { 
          vue: 'Vue',
          axios: 'axios',
          sweetalert2: 'Swal',
          pinia: 'pinia',
		      maska: 'maska',
          'vue-filepond': 'VueFilePond',
          'filepond-plugin-file-validate-type': 'filepondPluginFileValidateType',
          'filepond-plugin-file-validate-size': 'filepondPluginFileValidateSize',
          'filepond-plugin-image-preview': 'filepondPluginImagePreview',
          'filepond-plugin-pdf-preview': 'filepondPluginPdfPreview',
          'fast-xml-parser': 'fastXmlParser',
          'vue-currency-input': 'vueCurrencyInput'
        } 
      }
    },
    minify: "esbuild",
    esbuild: {
      drop: ['console', 'debugger'],
     }
  },
})
