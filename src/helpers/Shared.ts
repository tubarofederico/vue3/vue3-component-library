export function isEmpty(data: any): boolean {
    try {
        return (
            data == null ||
            typeof data === "undefined" ||
            (typeof data === "string" && data.trim() === "") ||
            (typeof data === "number" && Number.isNaN(data)) ||
            (Array.isArray(data) && data.length == 0) ||
            (typeof data === "object" &&
                data &&
                Object.keys(data).length === 0 &&
                Object.getPrototypeOf(data) === Object.prototype)
        );
    } catch (error) {
        throw error;
    }
}

export function getRouteNumberParam(param: string | string[]): number {
    let rta: number;
    if (typeof param === "string") {
        rta = parseInt(param);
    } else if (typeof param === "object") {
        rta = parseInt(param[0]);
    } else {
        throw new Error("Imposible parsear el parámetro");
    }
    return rta;
}

export function isNumber(value: string) {
    return /^\d*$/.test(value);
}

export function isPair(value: number): boolean {
    return value % 2 == 0;
}

export function checkParams(
    paramsRequest: Object,
    allParamsNames: String[]
): boolean {
    const paramsRequestNames: String[] =
        Object.getOwnPropertyNames(paramsRequest);
    const mergedNames: String[] = [
        ...new Set([...allParamsNames, ...paramsRequestNames]),
    ];
    const equalsCheck: (a: String[], b: String[]) => boolean = (a, b) =>
        a.length === b.length && a.every((v, i) => v === b[i]);
    return equalsCheck(allParamsNames, mergedNames);
}
