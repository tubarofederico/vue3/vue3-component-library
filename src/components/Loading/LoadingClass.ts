import type { Ref } from "vue";

export default class LoadingClass {
    loading!: Ref<boolean>;
    updateLoading!: Function
} 