export class StoreProceduresOptions {
    // Párametros SP alta
    prefijo_escan: string | null;
    id_tipo_doc_escan: number | null;
    id_tabla: number | null;
    id_tabla_reg: number | null;
    // Párametros para Listar
    id_rel_tabla_reg: number | null;

    constructor(
        prefijo_escan: string | null = null,
        id_tipo_doc_escan: number | null = null,
        id_tabla: number | null = null,
        id_tabla_reg: number | null = null,
        id_rel_tabla_reg: number | null = null
    ) {
        this.prefijo_escan = prefijo_escan;
        this.id_tipo_doc_escan = id_tipo_doc_escan;
        this.id_tabla = id_tabla;
        this.id_tabla_reg = id_tabla_reg;
        this.id_rel_tabla_reg = id_rel_tabla_reg;
    }
}

export class ServerConfig {
    process: string = "process"; // POST
    load: string = "load"; // POST
    remove: string = "remove"; // POST
    revert: string = "revert"; // POST
}

export class FileDBResponse {
    d_tipo_doc_escan!: string;
    clave!: string;
    f_segu!: string;
    f_segu_mostrar!: string;
    arch_ruta!: string;
    arch_nombre!: string;
    arch_extension!: string;
    arch_mostrar!: string;
    id_doc_escan!: string;
    ruta!: string;

    // constructor(
    //     d_tipo_doc_escan: string,
    //     clave: string,
    //     f_segu: string,
    //     f_segu_mostrar: string,
    //     arch_nombre: string,
    //     arch_extension: string,
    //     arch_mostrar: string,
    //     id_doc_escan: string,
    //     ruta: string
    // ) {
    //     this.d_tipo_doc_escan = d_tipo_doc_escan;
    //     this.clave = clave;
    //     this.f_segu = f_segu;
    //     this.f_segu_mostrar = f_segu_mostrar;
    //     this.arch_nombre = arch_nombre;
    //     this.arch_extension = arch_extension;
    //     this.arch_mostrar = arch_mostrar;
    //     this.id_doc_escan = id_doc_escan;
    //     this.ruta = ruta;
    // }
}