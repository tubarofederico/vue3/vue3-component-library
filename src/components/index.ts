// FILEUPLOADER
import FileUploader from './FileUploader/FileUploader.vue';
import { FileDBResponse, StoreProceduresOptions, ServerConfig } from './FileUploader/fileuploader';

//BREADCRUMB
import { BreadcrumbPlugin, BreadCrumbElement, BreadCrumbRoute, BreadCrumbKey } from './Breadcrumb/breadcrumb';

//LOADING
import Loading from './Loading/Loading';
import LoadingComponent from './Loading/LoadingComponent.vue';
import LoadingClass from './Loading/LoadingClass';

//NAVBAR
import NavBar from './Navbar/NavBar.vue';
import Option, { navbarKey } from './Navbar/navbar';

//AUTOCOMPLETE
import AutoComplete from './Autocomplete/AutoComplete.vue';
import AutocompleteElement from './Autocomplete/AutocompleteElement';

//DOCUMENTS LIST
import { DocumentsListPlugin, documentsListKey, DocumentsListFile, DocumentsListComponent } from './DocumentsList/documentslist';

//MODAL
import Modal from './Modal/Modal.vue';

//INPUT EXPEDIENTE
import InputExpediente from './InputExpediente/InputExpediente.vue';
import { Expediente } from './InputExpediente/Expediente';
import { useExpedientesStore } from './InputExpediente/expedientes';

//INPUT CURRENCY
import InputCurrency from './InputCurrency/InputCurrency.vue';
//import type { CurrencyInputOptions, CurrencyDisplay, NumberRange, ValueScaling } from "vue-currency-input";

//INPUT CUIL
import Cuil from './InputCuil/Cuil';
import InputCuil from './InputCuil/InputCuil.vue';

export {
    // FILEUPLOADER
    FileUploader,
    StoreProceduresOptions,
    FileDBResponse,
    ServerConfig,
    //BREADCRUMB
    BreadcrumbPlugin,
    BreadCrumbElement,
    BreadCrumbRoute,
    BreadCrumbKey,
    //LOADING
    Loading,
    LoadingComponent,
    LoadingClass,
    //NAVBAR
    NavBar,
    Option,
    navbarKey,
    //AUTOCOMPLETE
    AutoComplete,
    AutocompleteElement,
    //DOCUMENTS LIST
    DocumentsListPlugin,
    documentsListKey,
    DocumentsListFile,
    DocumentsListComponent,
    //MODAL
    Modal,
    //INPUT EXPEDIENTE
    InputExpediente,
    Expediente,
    useExpedientesStore,
    //INPUT CURRENCY
    InputCurrency,
    /*
    type CurrencyInputOptions,
    type CurrencyDisplay,
    type NumberRange,
    type ValueScaling,
    */
    //INPUT CUIL
    Cuil,
    InputCuil
}

