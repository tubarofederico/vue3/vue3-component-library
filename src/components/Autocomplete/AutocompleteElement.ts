export default abstract class AutocompleteElement {
    public abstract get key(): number | string;
    public abstract get value(): string;
}