import { checkParams, isEmpty, isNumber } from "../../index";

export default class Cuil {

	cuil: string = "";

	constructor(cuil: string = "") {
		this.setCuil(cuil);
	}

	validarCuil(nroDoc?: string): boolean {
		const cuilFormateado = this.getCuilFormateado();
		const cuilNroDoc = this.getNroDoc();
		const cuilDigitoVerif = Number(this.getDigitoVerif());
		const coeficientes = [5, 4, 3, 2, 7, 6, 5, 4, 3, 2];
		let suma: number = 0;
		let resultado: number;
		// Verificar que el CUIT tenga 11 dígitos
		if (cuilFormateado.length !== 11) {
			return false;
		}
		// Verificar que todos los caracteres sean numéricos
		if (!isNumber(cuilFormateado)) {
			return false;
		}
		// Verificar el documento
		if ((!isEmpty(nroDoc)) && (nroDoc !== cuilNroDoc)) {
			return false;
		}
		// Verificar el dígito verificador
		for (let i = 0; i < 10; i++) {
			suma += parseInt(cuilFormateado.charAt(i), 10) * coeficientes[i];
		}
		resultado = 11 - (suma % 11);
		if (resultado === 11) {
			resultado = 0;
		} else if (resultado === 10) {
			resultado = 9;
		}
		return (resultado === cuilDigitoVerif);
	}

	clearCuil() {
		this.cuil = "";
	}

	setCuil(cuil: string) {
		this.cuil = cuil;
	}

	getCuil(): string {
		return this.cuil;
	}

	getCuilFormateado(): string {
		return this.cuil.replace(/[-\s.]/g, '');
	}

	getGenero(): string | null {
		const cuilFormateado = this.getCuilFormateado();
		return (cuilFormateado.length === 11) ? cuilFormateado.substring(0, 2) : null;
	}

	getNroDoc(): string | null {
		const cuilFormateado = this.getCuilFormateado();
		return (cuilFormateado.length === 11) ? cuilFormateado.substring(2, 10) : null;
	}

	getDigitoVerif(): string | null {
		const cuilFormateado = this.getCuilFormateado();
		return (cuilFormateado.length === 11) ? cuilFormateado.charAt(10) : null;
	}

}
