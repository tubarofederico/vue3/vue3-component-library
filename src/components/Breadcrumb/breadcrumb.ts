import BreadCrumb from './Breadcrumb.vue';
import type { RouteParams, Router } from "vue-router";
import type { InjectionKey, Ref } from 'vue';

let BreadcrumbPlugin = {
    install: (app:any, options:any) => {

        // Registro el template
        app.component('BreadCrumb', BreadCrumb);
      
        // Expongo la instancia de VueRouter con provide al componente
        let router:Router = options.router;
        app.provide('routerBreadcrumb', router);
    }
};

class BreadCrumbRoute {
    name!: string;
    params?: RouteParams

    constructor(name: string, params: RouteParams | undefined = undefined) {
        this.name = name;
        this.params = params;
    }
}

class BreadCrumbElement {
    routes: BreadCrumbRoute[] = [];

    initRoutes(routes: BreadCrumbRoute[]){
        this.routes = routes;
    }

    addRoute(route: BreadCrumbRoute){
        this.routes.push(route);
    }

    getRoutes(): BreadCrumbRoute[]{
        return this.routes;
    }
    
}

const BreadCrumbKey = Symbol() as InjectionKey<Ref<BreadCrumbElement>>

export {
    BreadcrumbPlugin,
    BreadCrumbElement,
    BreadCrumbRoute,
    BreadCrumbKey
};