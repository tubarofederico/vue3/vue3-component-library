import { XMLParser, XMLValidator } from 'fast-xml-parser';
import { isEmpty } from "../../index";

/*
export interface ExpedienteInterface {
	id: number | null;
	origen: string | undefined;
	numero: number | undefined;
	anio: string | undefined;
	alcance: number | undefined;
	clave_exp: number | undefined;
	datos: string;

	getDatosExpediente(xmlData: string): object;
}
*/

//export class Expediente implements ExpedienteInterface {
export class Expediente {
	//id!: number | null;
	id?: number | null;
	origen?: string | null;
	numero?: number | null;
	anio?: string | null;
	alcance?: number | null;
	//clave_exp?: string | null;
	clave?: string | null;
	f_inicio?: string | null;
	interesado?: string | null;
	asunto?: string | null;
	tipo?: string | null;
	//datos!: string;
	//datos?: object | string | null;
	datos?: Record<string, any> | string | null;

	constructor(expediente: Expediente) {
		this.id = expediente.id;
		if (!isEmpty(expediente.clave)) {
			this.anio = expediente.clave!.substring(0, 4);
			this.origen = expediente.clave!.substring(5, 8);
			this.numero = Number(expediente.clave!.substring(9, 16));
			this.alcance = Number(expediente.clave!.substring(17));
		} else {
			this.anio = expediente.anio;
			this.origen = expediente.origen;
			this.numero = expediente.numero;
			this.alcance = expediente.alcance;
		}
		this.clave = expediente.clave;
		this.f_inicio = expediente.f_inicio;
		this.interesado = expediente.interesado;
		this.asunto = expediente.asunto;
		this.tipo = expediente.tipo;
		// @ts-ignore
		this.datos = (!isEmpty(expediente.datos) && XMLValidator.validate(String(expediente.datos))) ? new XMLParser().parse(String(expediente.datos)).expediente : {};
	}

	/*
	constructor(expe: any) {
		this.id = expe.id;
		this.origen = expe.origen;
		this.numero = expe.numero;
		this.anio = expe.anio;
		this.alcance = expe.alcance;
		//this.clave_exp = expe.clave_exp;
		this.clave = expe.clave_exp;
		//this.datos = this.getDatosExpediente(('datos' in expe)? expe.datos : "");
		this.datos = ('datos' in expe) ? this.getDatosExpediente(expe.datos) : {};
	}

	getDatosExpediente(xmlData: string): object {
		if (XMLValidator.validate(xmlData)) {
			const parser = new XMLParser();
			return parser.parse(xmlData).expediente;
		}
		return {};
	}

	decodeDatos() {
		this.datos = () => (!isEmpty(this.datos) && XMLValidator.validate(xmlData)) ? new XMLParser().parse(this.datos).expediente : {};
	}

	// toString(): string {
	// 	return (
	// 		this.origen_exp +
	// 		"-" +
	// 		this.numero_exp +
	// 		" / " +
	// 		this.anios_exp +
	// 		" " +
	// 		this.alcance
	// 	);
	// }

	decodeClave() {
		this.anio = this.clave.substring(0, 4);
		this.origen = this.clave.substring(5, 8);
		this.numero = Number(this.clave.substring(9, 16));
		this.alcance = Number(this.clave.substring(17));
	}
	*/
}
