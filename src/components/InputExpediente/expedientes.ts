import { defineStore } from "pinia";
import { ref } from "vue";
//import http from "@/http";
import axios from 'axios';
import Swal from "sweetalert2/dist/sweetalert2.js";
import type { AxiosResponse, AxiosError } from "axios";
import type { SweetAlertOptions } from "sweetalert2";
import { checkParams, isEmpty, isNumber } from "../../index";

// Entities
import { Expediente, /*type ExpedienteInterface*/ } from "../../index";

export const useExpedientesStore = defineStore("expedientes", () => {

  const urlServer = ref(null as string | null);
  const expedientes = ref([] as Expediente[]);
  const expediente = ref(null as Expediente | null);

  function getUrlServer(): string | null {
    return urlServer.value;
  }

  function setUrlServer(newUrlServer: string | null) {
    urlServer.value = newUrlServer;
  }

  function clearExpedientes() {
    expedientes.value = [];
  }

  function clearExpediente() {
    expediente.value = null;
  }

  function getExpedientes(): Expediente[] {
    return expedientes.value;
  }

  function getExpediente(): Expediente | null {
    return expediente.value;
  }

  async function fetchExpedientes(paramsRequest: Object = {}): Promise<boolean> {
    const allParamsNames: String[] = ["tipo", "id", "origen", "numero", "anio", "alcance"];
    const swal = {
      icon: "error",
      title: "Error",
      html: "",
    } as SweetAlertOptions;
    let isSuccess: boolean = true;
    clearExpedientes();
    if (!checkParams(paramsRequest, allParamsNames)) {
      swal.html =
        "Los par&aacute;metros enviados para la obtenci&oacute;n del listado de los expedientes son incorrectos. Por favor, avise al Administrador.";
      Swal.fire(swal);
      return false;
    }
    await axios.get<Expediente[]>(urlServer.value + "getExpedientes", { params: paramsRequest })
      .then(function (response: AxiosResponse<Expediente[]>) {
        expedientes.value = response.data;
      })
      .catch(function (error: AxiosError<string>) {
        if (error.response) {
          // Request made and server responded
          if (error.response.data.indexOf("|") !== -1) {
            swal.html = error.response.data.substring(
              error.response.data.indexOf("|") + 2,
              error.response.data.indexOf("Código") - 8
            );
          } else {
            swal.html =
              error.response.data.substring(
                error.response.data.indexOf("<h2>") + ("<h2>").length,
                error.response.data.indexOf("</h2>")
              );
          }
          console.log("Error: request made and server responded");
          console.log(swal.html);
          console.log(error.response);
          //console.log(error.response.status);
          //console.log(error.response.headers);
          //console.log(error.response.data);
        } else if (error.request) {
          // The request was made but no response was received
          swal.html =
            "Error desconocido. Por favor, avise al Administrador.";
          console.log("Error: the request was made but no response was received");
          console.log(error.request);
        } else {
          // Something happened in setting up the request that triggered an Error
          swal.html =
            "Error desconocido. Por favor, avise al Administrador.";
          console.log(
            "Error: something happened in setting up the request that triggered an error"
          );
          console.log(error);
        }
        Swal.fire(swal);
        isSuccess = false;
      });
    return isSuccess;
  };

  async function fetchExpediente(paramsRequest: Object = {}): Promise<boolean> {
    const allParamsNames: String[] = ["tipo", "id", "origen", "numero", "anio", "alcance"];
    const swal = {
      icon: "error",
      title: "Error",
      html: "",
    } as SweetAlertOptions;
    let isSuccess: boolean = true;
    clearExpediente();
    if (!checkParams(paramsRequest, allParamsNames)) {
      swal.html =
        "Los par&aacute;metros enviados para la obtenci&oacute;n del expediente son incorrectos. Por favor, avise al Administrador.";
      Swal.fire(swal);
      return false;
    }
    await axios.get<Expediente>(urlServer.value + "getExpediente", { params: paramsRequest })
      .then(function (response: AxiosResponse<Expediente>) {
        expediente.value = !isEmpty(response.data) ? response.data : null;
        isSuccess = !isEmpty(response.data);
      })
      .catch(function (error: AxiosError<string>) {
        if (error.response) {
          // Request made and server responded
          if (error.response.data.indexOf("|") !== -1) {
            swal.html = error.response.data.substring(
              error.response.data.indexOf("|") + 2,
              error.response.data.indexOf("Código") - 8
            );
          } else {
            swal.html =
              error.response.data.substring(
                error.response.data.indexOf("<h2>") + ("<h2>").length,
                error.response.data.indexOf("</h2>")
              );
          }
          console.log("Error: request made and server responded");
          console.log(swal.html);
          console.log(error.response);
          //console.log(error.response.status);
          //console.log(error.response.headers);
          //console.log(error.response.data);
        } else if (error.request) {
          // The request was made but no response was received
          swal.html =
            "Error desconocido. Por favor, avise al Administrador.";
          console.log("Error: the request was made but no response was received");
          console.log(error.request);
        } else {
          // Something happened in setting up the request that triggered an Error
          swal.html =
            "Error desconocido. Por favor, avise al Administrador.";
          console.log(
            "Error: something happened in setting up the request that triggered an error"
          );
          console.log(error);
        }
        Swal.fire(swal);
        isSuccess = false;
      });
    return isSuccess;
  };

  async function findExpediente(
    paramsSearch: { origen: string, numero: number, anio: string, alcance: number },
    forceUpdate: boolean = false,
    paramsUpdate: Object = {}
  ): Promise<Expediente | null> {
    const swal: SweetAlertOptions = {
      icon: "error",
      title: "Error",
      html: ""
    };
    let goFind: boolean = true;
    let expediente: Expediente | undefined = undefined;
    if (isEmpty(getExpedientes()) || forceUpdate) {
      goFind = await fetchExpedientes(paramsUpdate);
    }
    if (goFind) {
      expediente = getExpedientes().find((expediente: Expediente) => {
        return expediente.origen === paramsSearch.origen && expediente.numero === paramsSearch.numero &&
          expediente.anio === paramsSearch.anio && expediente.alcance === paramsSearch.alcance;
      });
    }
    /*
    if (isEmpty(expediente)) {
      swal.html =
        "No se encontr&oacute; en el sistema el expediente buscado. Por favor, avise al Administrador.";
      Swal.fire(swal);
    }
    */
    return new Promise((resolve) => {
      resolve((typeof expediente !== "undefined") ? new Expediente(expediente) : null);
    });
  };

  async function findExpedienteById(id: number, forceUpdate: boolean = false, paramsUpdate: Object = {}): Promise<Expediente | null> {
    const swal: SweetAlertOptions = {
      icon: "error",
      title: "Error",
      html: ""
    };
    let goFind: boolean = true;
    let expediente: Expediente | undefined = undefined;
    if (isEmpty(getExpedientes()) || forceUpdate) {
      goFind = await fetchExpedientes(paramsUpdate);
    }
    if (goFind) {
      expediente = getExpedientes().find((expediente: Expediente) => {
        return expediente.id === id;
      });
    }
    if (isEmpty(expediente)) {
      swal.html =
        "No se encontr&oacute; en el sistema el expediente buscado. Por favor, avise al Administrador.";
      Swal.fire(swal);
    }
    return new Promise((resolve) => {
      resolve((typeof expediente !== "undefined") ? new Expediente(expediente) : null);
    });
  };

  return {
    getUrlServer, setUrlServer, clearExpedientes, clearExpediente, getExpedientes, getExpediente,
    fetchExpedientes, fetchExpediente, findExpediente, findExpedienteById
  };

});
