import type { RouteLocationRaw } from "vue-router";
import type { InjectionKey, Ref } from 'vue';

export const navbarKey = Symbol() as InjectionKey<Ref<Option | null>>;

export default class Option {
  options!: Option[] | null;
  route!: RouteLocationRaw | null;
  name!: string;
  extra_db_data!: any;

  getIdOpcion(){
    return this.extra_db_data.id_opcion;
  }

  getIdApp(){
    return this.extra_db_data.id_app;
  }
}