import type { InjectionKey } from 'vue';
import DocumentsList from './DocumentsList.vue';
// import moment from 'moment';

export abstract class DocumentsListFile {
    id_doc_escan!: number;
    ruta!: string;
    arch_mostrar!: string;
    d_tipo_doc_escan!: string;
    f_segu!: string;
}

export const documentsListKey = Symbol() as InjectionKey<{
    base_url: string,
    title: string
}>;

import type Swal from "sweetalert2/dist/sweetalert2.js";
export const sweetalertKey = Symbol() as InjectionKey<typeof Swal>;

let DocumentsListPlugin = {
    install: (app: any, options: any) => {

        // Registro el template
        app.component('list-files', DocumentsList);
        // app.use(options.sweetalert)

        // Configuro las opciones generales
        if(typeof options.base_url == 'undefined' || options.base_url == ''){
            throw "Debe indicar el parámetro 'base_url' en el Plugin Listado de Imágenes";
        }

        const hcdListOptions = {
            base_url: options.base_url,
            title: "Listado"
        };

        app.provide(documentsListKey, hcdListOptions);
        app.provide(sweetalertKey, options.sweetalert);

        // Métodos comunes // equivalente a los filters de vue 2
        // app.config.globalProperties.$filters = {
        //     formatDate(value:any) {
        //         // return moment(String(value)).format('DD/MM/YYYY')
        //         return value;
        //     }
        // }
    }
}

export {
    DocumentsListPlugin,
    DocumentsList as DocumentsListComponent
}; 